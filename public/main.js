function saveLocalStorage() {
    localStorage.setItem("name", $("#name").val());
    localStorage.setItem("email", $("#email").val());
    localStorage.setItem("field-date", $("#field-date").val());
    localStorage.setItem("gender", $("#gender").val());
    localStorage.setItem("limbs", $("#limbs").val());
    localStorage.setItem("superpow", $("#superpow").val());
    localStorage.setItem("biography", $("#biography").val());
    localStorage.setItem("contract", $("#contract").prop("checked"));
}

function loadLocalStorage() {
    if (localStorage.getItem("name") !== null) {
        $("#name").val(localStorage.getItem("name"));
    }
    if (localStorage.getItem("email") !== null) {
        $("#email").val(localStorage.getItem("email"));
    }
    if (localStorage.getItem("field-date") !== null) {
        $("#field-date").val(localStorage.getItem("field-date"));
    }
    if ((localStorage.getItem("gender1") !== null)&&(localStorage.getItem("limbs2") !== null)) {
        $("#gender").val(localStorage.getItem("gender"));
    }
    if ((localStorage.getItem("limbs1") !== null)&&(localStorage.getItem("limbs2") !== null)&&(localStorage.getItem("limbs3") !== null)&&(localStorage.getItem("limbs4") !== null)&&(localStorage.getItem("limbs5") !== null)) {
        $("#limbs").val(localStorage.getItem("limbs"));
    }
    if (localStorage.getItem("superpow") !== null) {
        $("#superpow").val(localStorage.getItem("superpow"));
    }
    if (localStorage.getItem("biography") !== null) {
        $("#biography").val(localStorage.getItem("biography"));
    }
    if (localStorage.getItem("contract") !== null) {
        $("#contract").prop("checked", localStorage.getItem("contract") === "true");
        if ($("#contract").prop("checked")) {
            $("#send").removeAttr("disabled");
        }
    }
}
function clear() {
    localStorage.clear();
    $("#name").val("");
    $("#email").val("");
    $("#field-date").val("");
    $("#gender").val("");
    $("#limbs").val("");
    $("#superpow").val("");
    $("#biography").val("");
    $("#contract").val(false);
}

$(document).ready(function () {
    loadLocalStorage();
    $("a.button9").click(function () {
        $(".fixed-overlay").css("display", "flex");
        history.pushState(true, "", "./form");
    });
    $("#close").click(function () {
        $(".fixed-overlay").css("display", "none");
        history.pushState(false, "", ".");
 });
    $("#form").submit(function (e) {
        e.preventDefault();
        $(".fixed-overlay").css("display", "none");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "https://formcarry.com/s/1jcJiVUhVF",
            data: $(this).serialize(),
            success: function (response) {
                if (response.status === "success") {
                    alert("Your form has been submitted successfully");
                    clear();
                } else {
                    alert("Error: " + response.message);
                }
            }
        });
});
$("#contract").change(function(){
    if (this.checked) {
        $("#send").removeAttr("disabled");
    } else{
        $("#send").attr("disabled", "");
    }
});
$("#form").change(saveLocalStorage);
window.onpopstate = function(event){
    if (event.state){
        $(".fixed-overlay").css("display", "flex");
    } else{
        $(".fixed-overlay").css("display", "none");
    }
};
});

